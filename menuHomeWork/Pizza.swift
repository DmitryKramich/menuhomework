import UIKit

class Pizza: NSObject {
    
    var namePizza: String = ""
    var imagePizza: UIImage?
    var compositionPizza: String?
    var price: String?
    
    init(namePizza: String) {
        self.namePizza = namePizza
    }
    
    init(namePizza: String, price: String) {
        self.namePizza = namePizza
        self.price = price
    }
    
    init(namePizza: String, imagePizza: UIImage, compositionPizza: String) {
        self.namePizza = namePizza
        self.imagePizza = imagePizza
        self.compositionPizza = compositionPizza
    }
    
    init(namePizza: String, imagePizza: UIImage, compositionPizza: String, price: String) {
        self.namePizza = namePizza
        self.imagePizza = imagePizza
        self.compositionPizza = compositionPizza
        self.price = price
    }
}
