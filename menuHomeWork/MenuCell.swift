import UIKit

protocol MenuCellDelegate: AnyObject {
    func addToBasket(pizza: Pizza)
}

class MenuCell: UITableViewCell {

    @IBOutlet weak var imagePizzaView: UIImageView!
    @IBOutlet weak var namePizzaLabel: UILabel!
    @IBOutlet weak var compositionOfPizzaLabel: UILabel!
    @IBOutlet weak var pizzaPriceLabel: UILabel!
    @IBOutlet weak var sellButton: UIButton!
    private var pizza: Pizza?
    
    weak var delegate: MenuCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setup(with contact: Pizza) {
        pizza = contact
        imagePizzaView.image = contact.imagePizza
        namePizzaLabel.text = contact.namePizza
        pizzaPriceLabel.text = contact.price
        compositionOfPizzaLabel.text = contact.compositionPizza
        self.compositionOfPizzaLabel.numberOfLines = 0
    }
    
    @IBAction func sellButtonPressed(_ sender: UIButton) {
        guard let pizza = pizza else {
            return
        }
        delegate?.addToBasket(pizza: pizza)
    }
}
