import UIKit

class Detailed: UIViewController {

    @IBOutlet weak var pizzaImageView: UIImageView!
    @IBOutlet weak var priceTetField: UITextField!
    @IBOutlet weak var compositionPizzaLabel: UILabel!
    @IBOutlet weak var namePizzaTextField: UITextField!
    
    var pizza: Pizza?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setup()
        
    }
    
    private func setup () {
        self.namePizzaTextField.text = pizza?.namePizza
        self.pizzaImageView.image = pizza?.imagePizza
        self.priceTetField.text = pizza?.price
        self.compositionPizzaLabel.text = pizza?.compositionPizza
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        if let text = self.namePizzaTextField.text {
            self.pizza?.namePizza = text
        }
        if let text = self.priceTetField.text {
            self.pizza?.price = text
        }
        self.navigationController?.popViewController(animated: true)
    }
}
