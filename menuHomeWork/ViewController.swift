import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tabelView: UITableView!
    @IBOutlet weak var backetLabel: UILabel!
    
    
    var pizzasArray: [Pizza]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabelView.reloadData()
    }
    
    func initialSetup() {
        pizzasArray = ManagerMenu.shared.getPizza()
        tabelView.reloadData()
    }
}


extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pizzasArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as? MenuCell
            else {
                return UITableViewCell()
        }
        if let pizzasArray = pizzasArray {
            cell.setup(with: pizzasArray[indexPath.row])
            cell.delegate = self
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
  
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "Detailed") as? Detailed {
            if let pizzasArray = pizzasArray {
                controller.pizza = pizzasArray[indexPath.row]
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
}


extension ViewController: MenuCellDelegate {
    func addToBasket(pizza: Pizza) {
        guard let price = pizza.price else {
            return
        }
        
        backetLabel.text = pizza.namePizza + " " + price
    }
    
    
}



