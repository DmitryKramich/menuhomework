import UIKit

class ManagerMenu: NSObject {
    public static let shared = ManagerMenu()
    private override init() {}
    
    private let array:[Pizza] = [
        Pizza(namePizza: "Пять сыров",imagePizza: UIImage(named: "fiveCheese")!, compositionPizza: "Сыры: Моцарелла, Дор Блю, Проволоне, Фета, Чеддер; руккола, томатный соус, масло розмариновое, орегано", price: "15 р. 70 к."  ),
        Pizza(namePizza: "Цезарь",imagePizza: UIImage(named: "cesar")!, compositionPizza: "Филе куриное, томат, листья салата, соус Цезарь, сыр Пармезан, сыр Моцарелла, томатный соус, масло розмариновое, орегано", price: "13 р. 70 к"  ),
        Pizza(namePizza: "Супер пепперони",imagePizza: UIImage(named: "pepperoni")!, compositionPizza: "Очень много пепперони, острый перец халапеньо, сыр Моцарелла, томатный соус, масло розмариновое, орегано", price: "13 р. 70 к."  ),
        Pizza(namePizza: "Гриль Хаус",imagePizza: UIImage(named: "grill")!, compositionPizza: "Фарш говяжий Болоньезе, томат, листья салата, соус Бургер, красный лук, сыр Моцарелла, томатный соус для пиццы, масло розмариновое, орегано", price: "15 р. 70 к"  ),
        Pizza(namePizza: "Барбекю ",imagePizza: UIImage(named: "fiveCheese")!, compositionPizza: "Свинина барбекю, бекон, шампиньоны, карамельный лук, соус Барбекю, сыр Моцарелла, масло розмариновое, орегано", price: "15 р. 70 к."  ),
        ]
    
    func getPizza() ->[Pizza] {
        return array
    }
}
